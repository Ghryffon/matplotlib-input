import matplotlib.pyplot as pyplot

x_values = []
y_values = []
graph_bounds = []
point_num = input("How many points would you like to plot?")

for i in range(4):
    bound = input("enter graph bound: ")
    graph_bounds.append(bound)

final_bounds = (0,10,0,10)
print(final_bounds)

for i in point_num * 2:
    x = input("enter x value: ")
    x_values.append(x)
    y = input("enter y value: ")
    y_values.append(y)

pyplot.plot(x_values, y_values,"ro")

pyplot.axes(final_bounds)

pyplot.show()

